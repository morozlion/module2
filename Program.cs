using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Linq;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * tax / 100 * companyRevenue;
           
        }

        public static string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            { return "���������� � ����������������!"; }
            else if (input % 2 == 1 && input < 18 && input > 12)
            { return "���������� � ��������� � ������� �����!"; }
            else { return $"���������� � {input}-������!"; }
        }

        public static double GetMultipliedNumbers(string first, string second)
        {
            double x, y;
            Double.TryParse(first, NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out x);
            Double.TryParse(second, NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out y);
            //try
            //{
            //    x = Convert.ToDouble(first.Replace(',', '.'), System.Globalization.CultureInfo.InvariantCulture);
            //}
            //catch { throw new ArgumentException("�� �����"); }
            //try { 
            //    y = Convert.ToDouble(second.Replace(',', '.'), System.Globalization.CultureInfo.InvariantCulture);
            //}
            //catch { throw new ArgumentException("�� �����"); }
            return x * y;
        }
       public static double GetFigureValues(
           Figure figureType,
           Parameter parameterToCompute,
           Dimensions dimensions)
        {
                switch (figureType)
                {
                    case Figure.Triangle:
                        if (dimensions.Height != 0)
                        { return (double)(dimensions.FirstSide * dimensions.Height) / 2.0; }
                        else
                        {
                            double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2.0;
                            return (double)(Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide)));
                        }
                    case Figure.Rectangle:
                        if (Parameter.Square == parameterToCompute)
                            return (double)(dimensions.FirstSide * dimensions.SecondSide);
                        else return (double)((dimensions.FirstSide + dimensions.SecondSide) * 2);
                    case Figure.Circle:
                        if (Parameter.Square == parameterToCompute)
                            return Convert.ToDouble((Math.PI * dimensions.Radius * dimensions.Radius));
                        else return Convert.ToDouble((2 * Math.PI * dimensions.Radius));
                default:
                    throw new Exception();
                }
        }
    }
}
